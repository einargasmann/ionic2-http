# Learning Ionic 2 - Fetching remote data #
http, .map, .subsribe, ion-list, *ngFor 

Using Ionic http service to fetch data from the Reddit API

### Used in this tutorial: 

* Http-service with .get
* .map operator to decode the json-response
* .subscribe to retrieve the decoded response
* ion-list with *ngFor to list the response

Tutorial link:  
http://www.joshmorony.com/using-http-to-fetch-remote-data-from-a-server-in-ionic-2/