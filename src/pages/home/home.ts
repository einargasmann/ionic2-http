import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  posts: any;

  constructor(public http: Http) {
    //.get returns a request in the for of an Observable
    //.map modifies the json-response by decoding it
    //.subscribe then access that modified data and hands it over to this.posts
    this.http.get('https://www.reddit.com/r/gifs/new/.json?limit=10')
      .map(res => res.json())
      .subscribe(
        data => {
          this.posts = data.data.children;
        }, 
        error => {
          console.log(error)
        });
  }
}
